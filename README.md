# BXC_AudioDemo
@author 北小菜 https://www.bilibili.com/video/BV1ov4y1R789

## 本项目为一个音频相关的demo：
1.  decode_aac.cpp           fdk-aac实现解码aac文件
2.  sdl2_play_aac.cpp        fdk-aac实现解码aac文件，SDL2实现播放
3.  sdl2_play_pcm.cpp      SDL2实现播放pcm文件


### windows系统编译运行
~~~
只需要使用vs2019打开 BXC_AudioDemo.sln
选择 x64/Debug 或 x64/Release都能够直接运行，第三方依赖库包括SDL2和fdk-aac，已经配置成相对路径
vs2017,vs2022 没有试，应该都没有问题
 
注意：
1，上面三个cpp文件分别都有main函数，都可以作为入口函数，所以在使用的时候，一定要将其余两个cpp文件设置为不参与编译
2，运行demo的时候需要将，根目录下\3rdparty\SDL2\bin\SDL2.dll拷贝到 根目录下\x64\Release 或 根目录下\x64\Debug

~~~




